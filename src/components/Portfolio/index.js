import bit_bucket_icon from "../../assets/bit_bucket_icon.png";
import external_link_icon from "../../assets/external_link_icon.png";
import portfolio_case_01 from "../../assets/portfolio_case_01.png";
import portfolio_case_02 from "../../assets/portfolio_case_02.png";
import portfolio_case_03 from "../../assets/portfolio_case_03.png";
import portfolio_case_04 from "../../assets/portfolio_case_04.png";
import portfolio_case_05 from "../../assets/portfolio_case_05.png";
import portfolio_case_06 from "../../assets/portfolio_case_06.png";

const Portfolio = () => {
  return (
    <div id="portfolio" className="main-porftolio-wrapper">
      <p className="description__p1">My works</p>
      <p className="description__p2">Portfolio</p>
      <div className="portfolio-case-wrapper">
        <div className="portfolio__case">
          <img src={portfolio_case_01} alt=""/>
          <div className="portfolio__button">
            <img src={bit_bucket_icon} alt=""/>
          </div>
          <div className="portfolio__button">
            <img src={external_link_icon} alt=""/>
          </div>
        </div>
        <div className="portfolio__case">
          <img src={portfolio_case_02} alt=""/>
          <div className="portfolio__button">
            <img src={bit_bucket_icon} alt=""/>
          </div>
          <div className="portfolio__button">
            <img src={external_link_icon} alt=""/>
          </div>
        </div>
        <div className="portfolio__case">
          <img src={portfolio_case_03} alt=""/>
          <div className="portfolio__button">
            <img src={bit_bucket_icon} alt=""/>
          </div>
          <div className="portfolio__button">
            <img src={external_link_icon} alt=""/>
          </div>
        </div>
        <div className="portfolio__case">
          <img src={portfolio_case_04} alt=""/>
          <div className="portfolio__button">
            <img src={bit_bucket_icon} alt=""/>
          </div>
          <div className="portfolio__button">
            <img src={external_link_icon} alt=""/>
          </div>
        </div>
        <div className="portfolio__case">
          <img src={portfolio_case_05} alt=""/>
          <div className="portfolio__button">
            <img src={bit_bucket_icon} alt=""/>
          </div>
          <div className="portfolio__button">
            <img src={external_link_icon} alt=""/>
          </div>
        </div>
        <div className="portfolio__case">
          <img src={portfolio_case_06} alt=""/>
          <div className="portfolio__button">
            <img src={bit_bucket_icon} alt=""/>
          </div>
          <div className="portfolio__button">
            <img src={external_link_icon} alt=""/>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Portfolio;
