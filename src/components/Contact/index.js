import ContactForm from "../ContactForm";
import myPhoto2 from "../../assets/myPhoto2.jpg";

const Contact = () => {
  return (
    <div id="contact" className="main-contact-wrapper">
      <div className="main-contact-container">
        <div className="main-contact__message">
          <p className="description__p1">Contact me</p>
          <p className="description__p3">
            If you are willing to work with me, contact me. I can join your
            conference to serve you with my engeneering skills.
          </p>
          <ContactForm />
        </div>
        <div className="main-contact__info">
          <div className="contact__info--data">
            <p>t.szklarczyk92@gmail.com</p>
            <p>+32 123 345 567</p>
          </div>
          <div className="contact__info--caption">
            <img src={myPhoto2} alt="" />
            <p>Author Tomasz Szklarczyk</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
