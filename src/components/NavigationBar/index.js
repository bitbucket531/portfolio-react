const NavigationBar = () => {
  return (
    <div className="main-nav-wrapper">
      <div className="main-nav__bar">
        <div className="nav-bar__element">
          <a href="#about-me">
            <span>About me</span>
          </a>
        </div>
        <div className="nav-bar__element">
          <a href="#skills">
            <span>Skills</span>
          </a>
        </div>
        <div className="nav-bar__element">
          <a href="#portfolio">
            <span>Portfolio</span>
          </a>
        </div>
        <div className="nav-bar__element">
          <a href="#contact">
            <span>Contact me</span>
          </a>
        </div>
      </div>
    </div>
  );
};

export default NavigationBar;
