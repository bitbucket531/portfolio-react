const AboutMe = () => {
    return (
        <div id="about-me" className="main-about-me-wrapper">
            <div>
                <p className="description__p1">About me</p>
                <p className="description__p3">I obtained the title of engineer on Silesian Univeristy of Technology in
                    Gliwice and my biggest dream is to become a programmer. Im interested in many things but mainly in
                    chess and IT.</p>
                <p className="description__p2">My interest</p>

                <ul>
                    <li>
                        <p className="description__p3">Chess</p>
                    </li>
                    <li>
                        <p className="description__p3">IT</p>
                    </li>
                    <li>
                        <p className="description__p3">Healthy life</p>
                    </li>
                    <li>
                        <p className="description__p3">Astronomy</p>
                    </li>
                </ul>

                <div className="about-me__finished-course">
                    <p>Ukoczyłem kurs Front-end Dev Hero.</p>
                </div>
            </div>
        </div>
    )
}

export default AboutMe;