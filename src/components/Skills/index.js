const Skills = () => {
  return (
    <div id="skills" className="main-skills-wrapper">
      <p className="description__p1">Skills</p>
      <ul className="skills-list">
        <li className="skills-list__element1">HTML 70%</li>
        <li className="skills-list__element2">CSS 60%</li>
        <li className="skills-list__element3">JavaScript 70%</li>
        <li className="skills-list__element4">GIT 50%</li>
        <li className="skills-list__element5">C# 90%</li>
        <li className="skills-list__element6">C++ 80%</li>
      </ul>
    </div>
  );
};

export default Skills;
