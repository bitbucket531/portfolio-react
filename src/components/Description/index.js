import kolo1 from "../../assets/koła_01.png";
import kolo2 from "../../assets/koła_02.png";
import myPhoto from "../../assets/myPhoto.jpg";

const Description = () => {
  return (
    <div className="main-description-wrapper">
      <img id="circle2" src={kolo1} alt="" />
      <img id="circle3" src={kolo2} alt="" />
      {/* <p class="test-red" style={{ textAlign: "center" }}>
        red
      </p>
      <p class="test-orange" style={{ textAlign: "center" }}>
        orange
      </p>
      <p class="test-blue" style={{ textAlign: "center" }}>
        blue
      </p> */}
      <div className="description__image-wrapper">
        <img src={myPhoto} alt="" />
      </div>
      <div className="description__welcome">
        <p className="description__p1">Hi, my name is Tomasz</p>
        <p className="description__p2">Begginer Front-end programmer</p>
        <p className="description__p3">
          Graduate of The Silesian University of Technology, ready to learn and
          gain experience
        </p>
      </div>
      <div className="description__welcome2">
        <p className="description__p4">
          Contact me if you want to work with me
        </p>
        <div className="description__hire">
          <p>
            <a className="description__button" href="a">
              Hire me
            </a>
          </p>
        </div>
        <div className="description__download">
          <p>
            <a className="description__button" href="a">
              Download CV
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Description;
