import React from "react";

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      message: "",
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <form
        action="./test/php"
        method="GET"
        target="blank"
        onSubmit={this.handleSubmit}
      >
        <div className="form__input">
          <input
            name="email"
            type="text"
            placeholder="Your e-mail"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </div>
        <div className="form__input">
          <input
            name="name"
            type="text"
            placeholder="Your name"
            value={this.state.name}
            onChange={this.handleChange}
          />
        </div>
        <div className="form__input">
          <textarea
            name="message"
            placeholder="How can i help you?"
            value={this.state.message}
            onChange={this.handleChange}
          ></textarea>
        </div>
        <div className="form__input">
          <input type="submit" value="Send" />
        </div>
      </form>
    );
  }
}

export default ContactForm;
