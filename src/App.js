import "./App.css";
import './styles/themes/default/theme.scss'
import Homepage from "./pages/homepage";
import NavigationBar from "./components/NavigationBar";
import Description from "./components/Description";
import AboutMe from "./components/AboutMe";
import Skills from "./components/Skills";
import Portfolio from "./components/Portfolio";
import Contact from "./components/Contact"

function App() {
  return (
    <Homepage>
      <NavigationBar />
      <Description />
      <AboutMe />
      <Skills />
      <Portfolio/>
      <Contact/>
    </Homepage>
  );
}

export default App;
